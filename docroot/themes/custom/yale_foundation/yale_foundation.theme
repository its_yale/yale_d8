<?php

/**
 * @file
 * Custom theme functions for yale_foundation.
 */

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;
use Drupal\Component\Utility\Html;
use Drupal\block\Entity\Block;

/**
 * Implements hook_preprocess_block().
 */
function yale_foundation_preprocess_block(&$variables) {
}

/**
 * Implements hook_form_alter().
 */
function yale_foundation_form_search_block_form_alter(&$form, $form_state) {

  $form['keys']['#theme_wrappers'][] = 'form_element__search';
  $form['keys']['#attributes']['placeholder'] = new TranslatableMarkup('Search this site');
}

/**
 * Implements template_preprocess_field().
 */
function yale_foundation_preprocess_field(&$variables) {

  // Format the carousel fields.
  $carousel_fields = ['field_yale_gallery_carousel', 'field_yale_carousel_slides'];
  if (in_array($variables['field_name'], $carousel_fields)) {

    // Provide the paragraph title to the slide item.
    foreach ($variables['items'] as $i => &$carousel_slide) {
      /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
      $paragraph = $carousel_slide['content']['#paragraph'];

      $title    = $paragraph->field_yale_carousel_title->value;
      $slide_id = Html::getId('slide-' . $i);

      $carousel_slide['title'] = $title;

      $carousel_slide['title_attributes'] = new Attribute();
      $carousel_slide['title_attributes']->setAttribute('id', $slide_id);

      $carousel_slide['attributes'] = new Attribute();
      $carousel_slide['attributes']->addClass('orbit-slide');
      $carousel_slide['attributes']->setAttribute('aria-labelledby', $slide_id);
    }
  }
}

/**
 * Implements template_preprocess_html().
 *
 * Adds additional classes.
 */
function yale_foundation_preprocess_html(&$variables) {
}

/**
 * Implements hook_preprocess_menu().
 */
function yale_foundation_preprocess_menu(&$variables) {

  if ($variables['menu_name'] === 'social') {

    // Wrap title of links with a FontAwesome class in screen-reader-only span.
    foreach ($variables['items'] as &$item) {
      $attributes = $item['url']->getOption('attributes');
      if (!empty($attributes['class']) && in_array('fa', $attributes['class'])) {
        $item['title'] = new FormattableMarkup('<span class="show-for-sr">@text</span>', ['@text' => $item['title']]);
      }
    }
  }
}

/**
 * Implements template_preprocess_node.
 *
 * Add template suggestions and classes.
 */
function yale_foundation_preprocess_node(&$variables) {
}

/**
 * Implements template_preprocess_page.
 *
 * Add convenience variables and template suggestions.
 */
function yale_foundation_preprocess_page(&$variables) {

  // Set the main grid.
  $sidebar_first = $variables['page']['sidebar_first'];
  $sidebar_second = $variables['page']['sidebar_second'];

  $has_first_sidebar  = _yale_foundation_sidebar_check($variables, $sidebar_first);
  $has_second_sidebar = _yale_foundation_sidebar_check($variables, $sidebar_second);

  switch (TRUE) {
    case (!$has_first_sidebar && $has_second_sidebar):
      $variables['main_grid']          = 'medium-9';
      $variables['sidebar_first_grid'] = '';
      $variables['sidebar_sec_grid']   = 'medium-3';
      unset($variables['page']['sidebar_first']);
      break;

    case ($has_first_sidebar && $has_second_sidebar):
      $variables['main_grid']          = 'medium-6 medium-push-3';
      $variables['sidebar_first_grid'] = 'medium-3 medium-pull-6';
      $variables['sidebar_sec_grid']   = 'medium-3';
      break;

    case ($has_first_sidebar && !$has_second_sidebar):
      $variables['main_grid']          = 'medium-9 medium-push-3';
      $variables['sidebar_first_grid'] = 'medium-3 medium-pull-9';
      $variables['sidebar_sec_grid']   = '';
      unset($variables['page']['sidebar_second']);
      break;

    case (!$has_first_sidebar && !$has_second_sidebar):
    default:
      $variables['main_grid']          = 'medium-12';
      $variables['sidebar_first_grid'] = '';
      $variables['sidebar_sec_grid']   = '';
  }

}

/**
 * Implements template_preprocess_views_view().
 */
function yale_foundation_preprocess_views_view(&$variables) {
}

/**
 * Prevents empty sidebars when they contain only an empty menu block.
 *
 * @param array $variables
 *   Variables array from preprocess page.
 * @param array $region
 *   The name of the region we're checking.
 *
 * @return bool
 *   Returns true if not empty, false otherwise.
 */
function _yale_foundation_sidebar_check(&$variables, $region) {
  $not_empty = FALSE;
  if (is_array($region)) {
    foreach ($region as $key => $item) {
      if ($block = Block::load($key)) {

        if ($block->getPlugin()->getBaseId() != 'system_menu_block') {
          $not_empty = TRUE;
        }
        else {
          $build = $block->getPlugin()->build();
          if (isset($build['#menu_name'])) {
            $not_empty = TRUE;
          }
        }
      }
    }
  }
  return $not_empty;
}
