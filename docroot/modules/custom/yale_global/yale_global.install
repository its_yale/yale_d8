<?php

/**
 * @file
 * Install, update and uninstall functions for the yale_global module.
 */

use Drupal\user\Entity\Role;

/**
 * Implements hook_install().
 */
function yale_global_install() {
  /** @var \Drupal\Core\Extension\ModuleInstallerInterface $moduleInstaller */
  $moduleInstaller = \Drupal::service('module_installer');

  // Install any additional modules.
  // These modules are not specified as dependencies in yale_global.info.yml
  // so that they can optionally be disabled.
  $moduleInstaller->install([
    'calendar',
    'cas',
    'search',
    'search_api',
    'search_api_db',
    'features',
    'features_ui',
    'pathauto',
    'restui',
    'xmlsitemap',
    'yale_ct_event',
    'yale_ct_alert',
    'yale_vc_person_type',
    'yale_ct_person',
    'yale_ct_gallery',
    'yale_ct_news',
    'yale_ct_landing_page',
  ]);

  // Modules can't rely on themes, so the theme must be enabled here instead of
  // in yale_global.info.yml.
  // Bartik is set as default in lightning_install() which will be run later, so
  // the default needs to be changed in yale_global_modules_installed() instead.
  /** @var \Drupal\Core\Extension\ThemeInstallerInterface $themeInstaller */
  $themeInstaller = \Drupal::service('theme_installer');
  $themeInstaller->install(['yale_foundation']);

  // Override some default CAS module settings to set Yale configurations.
  $config = \Drupal::service('config.factory')->getEditable('cas.settings');
  $config
    ->set('server.hostname', 'secure.its.yale.edu')
    ->set('server.path', '/cas')
    ->set('user_accounts.auto_register', TRUE)
    ->save();

  // Grant permissions to Anonymous and Authenticated users.
  /** @var Role[] $userRoles */
  $userRoles = Role::loadMultiple(['anonymous', 'authenticated']);
  foreach ($userRoles as $role) {
    $role->grantPermission('search content')->save();
  }

  // Create default social media links.
  $sociaLinks = [
    [
      'title' => 'Facebook',
      'link' => [
        'uri' => 'https://www.facebook.com/YaleUniversity',
        'options' => [
          'attributes' => ['class' => ['fa', 'fa-facebook-square']],
        ],
      ],
    ],
    [
      'title' => 'Twitter',
      'link' => [
        'uri' => 'http://www.twitter.com/yale',
        'options' => [
          'attributes' => ['class' => ['fa', 'fa-twitter']],
        ],
      ],
    ],
    [
      'title' => 'Youtube',
      'link' => [
        'uri' => 'http://www.youtube.com/yale',
        'options' => [
          'attributes' => ['class' => ['fa', 'fa-youtube-play']],
        ],
      ],
    ],
    [
      'title' => 'Tumblr',
      'link' => [
        'uri' => 'http://yaleuniversity.tumblr.com/',
        'options' => [
          'attributes' => ['class' => ['fa', 'fa-tumblr-square']],
        ],
      ],
    ],
  ];

  /** @var \Drupal\Core\Entity\EntityStorageInterface $menuLinkContentStorage */
  $menuLinkContentStorage = \Drupal::entityTypeManager()->getStorage('menu_link_content');

  $weight = 0;
  foreach ($sociaLinks as $sociaLink) {
    $menuLinkContentStorage->create(
      $sociaLink + [
        'menu_name' => 'social',
        'weight' => $weight++,
      ])->save();
  }

  // Update Main menu name to "Main menu" for accessibility concerns.
  $settings = \Drupal::service('config.factory')->getEditable('system.menu.main');
  $settings->set('label', 'Main menu')->save();

}
