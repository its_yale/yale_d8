<?php

namespace Drupal\yale_global\Controller;

use Drupal\search\Controller\SearchController;
use Drupal\search\SearchPageInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class YaleSearchController.
 *
 * @package Drupal\yale_global\Controller
 */
class YaleSearchController extends SearchController {

  /**
   * {@inheritdoc}
   */
  public function view(Request $request, SearchPageInterface $entity) {
    $build = parent::view($request, $entity);

    // Normalize the title, since it's inconsistently rendered.
    if (isset($build['search_results_title'])) {
      unset($build['search_results_title']);
    }

    $search_title['#markup'] = '<h1>' . $this->t('Search') . '</h1>';
    $build = ['search_title' => $search_title] + $build;

    // Remove the markup from the empty, since we're handling it in twig.
    $build['search_results']['#empty'] = [
      '#markup' => $this->t('Your search yielded no results.'),
    ];

    return $build;
  }

}
