Yale Alert
----------

Alert content type and views

## Notes

Features exports block configuration to `config/install`, but installation will
fail due to a dependency on the views configuration which is exported to
`config/optional`.  In order to install correctly, the block configuration must
be moved to `config/optional` as well.
